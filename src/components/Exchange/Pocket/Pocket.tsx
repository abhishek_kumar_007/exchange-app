import React, {FC} from "react";
import NumberField, {
    NumberFieldOnChangeFn,
    NumberInput,
    Props as NumberFieldProps
} from "../../shared/NumberField/NumberField";
import Wallet from "../../../models/Wallet";
import {Symbols} from "../../../models/CurrencySymbols";
import NativeSelect, {SelectOnChangeFn} from "../../shared/NativeSelect/NativeSelect";
import styles from "./pocket.module.scss";

export interface Props {
    wallet: Wallet;
    styles?: Object;
    symbols: Symbols;
    className?: string;
    /**
     * supports 3 digit code
     */
    selectedCurrency: string;
    exchangeRateInfo: string;
    currencyValue: NumberInput;
    inputProps?: NumberFieldProps;
    showExchangeFlowIcon?: boolean;
    onCurrencyChange?: SelectOnChangeFn;
    onCurrencyValueChange?: NumberFieldOnChangeFn;
}

const Pocket: FC<Props> = (props) => {
    const {
        wallet,
        symbols,
        className,
        inputProps,
        currencyValue,
        onCurrencyChange,
        exchangeRateInfo,
        styles: stylesProps,
        showExchangeFlowIcon,
        onCurrencyValueChange,
        selectedCurrency: selectCurrencyProps
    } = props;

    // get all currencies 3 digit code
    const symbols3Letter = Object.keys(symbols);
    let selectedCurrency = selectCurrencyProps;

    if (!selectedCurrency && selectedCurrency === "" && symbols3Letter.length) {
        selectedCurrency = symbols3Letter[0];
    }

    const options = symbols3Letter.map((code) => {
        return (
            <option key={code}>
                {code}
            </option>
        );
    });

    return (
        <div className={className} style={stylesProps}>
            {showExchangeFlowIcon ? <div className={styles.exchangeFlowIcon}/> : null}
            <div className={styles.pocketItemContainer}>
                <div className={styles.pocketItems}>
                    <NativeSelect
                        className={styles.currencySelect}
                        value={selectedCurrency}
                        onChange={onCurrencyChange}>
                        {options}
                    </NativeSelect>
                    <NumberField
                        {...inputProps}
                        className={styles.currencyField}
                        value={currencyValue}
                        onChange={onCurrencyValueChange}/>
                </div>
                <div className={styles.exchangeRate}>
                    <div>You have {wallet.getBalance()} {wallet.getWalletCurrency()}</div>
                    <div>{exchangeRateInfo}</div>
                </div>
            </div>
        </div>
    )
};

Pocket.defaultProps = {
    className: "",
    styles: {},
    showExchangeFlowIcon: false
};

export default React.memo(Pocket, (prevProps, nextProps) => {
    return prevProps.currencyValue === nextProps.currencyValue &&
        prevProps.symbols === nextProps.symbols &&
        prevProps.selectedCurrency === nextProps.selectedCurrency &&
        prevProps.exchangeRateInfo === nextProps.exchangeRateInfo &&
        prevProps.wallet === nextProps.wallet;
});