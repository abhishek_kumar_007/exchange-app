import {Dispatch} from "react";
import {BaseReduxAction} from "../types";
import {CurrenciesRateOptions} from "../models/CurrenciesRate";
import CurrencyExchangeRate from "../communicators/CurrencyExchangeRate";
import {FetchCurrenciesRateOptions} from "../communicators/CurrencySymbols";

export interface CurrenciesRateAction extends BaseReduxAction {
    currenciesRate?: CurrenciesRateOptions | null
}

export const REQUEST_CURRENCIES_RATE = "REQUEST_CURRENCIES_RATE";
export const RECEIVE_CURRENCIES_RATE = "RECEIVE_CURRENCIES_RATE";
export const INVALIDATE_CURRENCY_RATE = "INVALIDATE_CURRENCY_RATE";

const currencyExchangeRateCommunicator = new CurrencyExchangeRate();

export function invalidateCurrencyRate(): CurrenciesRateAction {
    return {
        type: INVALIDATE_CURRENCY_RATE
    }
}

export function requestCurrencyRate(): CurrenciesRateAction {
    return {
        type: REQUEST_CURRENCIES_RATE
    }
}

export function receiveCurrencyRate(currenciesRate?: CurrenciesRateOptions | null): CurrenciesRateAction {
    return {
        type: RECEIVE_CURRENCIES_RATE,
        currenciesRate
    }
}

export function fetchCurrenciesRate(options: FetchCurrenciesRateOptions) {
    return async (dispatch: Dispatch<Object>) => {
        dispatch(requestCurrencyRate());
        const response = await currencyExchangeRateCommunicator.getLatestCurrenciesValue(options);
        if (response) {
            dispatch(receiveCurrencyRate({
                timestamp: response.time_last_updated,
                base: response.base,
                date: response.date,
                rates: response.rates
            }));
        }
    }
}

export function fetchCurrenciesRateIfNeeded(options: FetchCurrenciesRateOptions) {
    return (dispatch: Dispatch<Object>) => {
        return dispatch(fetchCurrenciesRate(options));
    }
}