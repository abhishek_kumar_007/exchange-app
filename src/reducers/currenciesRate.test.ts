import reducer from "./currenciesRate";
import {
    requestCurrencyRate,
    receiveCurrencyRate
} from "../actions/currenciesRate";

describe("Currency Rate", () => {

    it("should return the initial state", () => {
        expect(reducer(undefined, {type: ""})).toEqual(
            {
                isFetching: false,
                didInvalidate: false,
                lastUpdated: null,
                currenciesRate: null
            }
        )
    });

    it("should request currencies rate", () => {
        expect(
            reducer(undefined, requestCurrencyRate())
        ).toEqual({
                isFetching: true,
                didInvalidate: false,
                lastUpdated: null,
                currenciesRate: null
            }
        );
    });

    it("should store currencies rate", () => {
        expect(
            reducer(
                undefined,
                receiveCurrencyRate({
                    base: "USD",
                    timestamp: "",
                    date: "",
                    rates: {"INR": 70}
                })
            )
        ).toEqual(
            {
                didInvalidate: false,
                isFetching: false,
                currenciesRate: {
                    base: "USD",
                    timestamp: "",
                    date: "",
                    rates: {"INR": 70}
                }
            }
        )
    })
});