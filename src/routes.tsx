import React, {Suspense} from "react";
import {Switch, HashRouter as Router, Route} from "react-router-dom";

const Exchange = React.lazy(() => import("./pages/Exchange/Exchange"));
const Transactions = React.lazy(() => import("./pages/Wallets/Wallets"));

export default function Routes() {
    return (
        <Suspense fallback={<div>Loading...</div>}>
            <Router>
                <Switch>
                    <Route exact path="/wallets">
                        <Transactions/>
                    </Route>
                    <Route exact path="/">
                        <Exchange/>
                    </Route>
                </Switch>
            </Router>
        </Suspense>
    );
}