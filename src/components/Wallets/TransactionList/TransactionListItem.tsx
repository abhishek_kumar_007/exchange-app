import React, {FC} from "react";
import {WalletTransaction} from "../../../models/Wallet";
import {convertTimeStampToDateTimeStr} from "../../../utils/utils";
import styles from "./transactionListItem.module.scss";

export interface Props {
    /**
     * supports 3 digit code
     */
    selectedCurrency: string,
    transaction: WalletTransaction
}

const TransactionListItem: FC<Props> = (props) => {
    const {selectedCurrency, transaction} = props;

    if (transaction && selectedCurrency) {
        let summaryText = "";
        let amount = "0";
        let exchangedAmount = "0";

        if (selectedCurrency === transaction.currency) {
            // amount deducted
            summaryText = `Exchanged to ${transaction.exchangedCurrency}`;
            amount = `- ${transaction.amount} ${transaction.currency}`;
            exchangedAmount = `+ ${transaction.exchangedAmount} ${transaction.exchangedCurrency}`;
        } else {
            // amount added in wallet
            summaryText = `Exchanged from ${transaction.currency}`;
            amount = `+ ${transaction.exchangedAmount} ${transaction.exchangedCurrency}`;
            exchangedAmount = `- ${transaction.amount} ${transaction.currency}`;
        }

        return (
            <div className={styles.transactionListItemSection}>
                <div className={styles.logo}>
                    <img alt="Exchange" src="https://img.icons8.com/flat_round/64/000000/available-updates--v1.png"/>
                </div>
                <div className={styles.description}>
                    <div>{summaryText}</div>
                    <div>{convertTimeStampToDateTimeStr(transaction.timestamp)}</div>
                </div>
                <div className={styles.amount}>
                    <div>{amount}</div>
                    <div>{exchangedAmount}</div>
                </div>
            </div>
        )
    }

    return null;
};

export default React.memo(TransactionListItem);