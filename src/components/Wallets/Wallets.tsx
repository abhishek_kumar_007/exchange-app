import React, {FC} from "react";
import {Link} from "react-router-dom";
import Wallet from "../../models/Wallet";
import Loader from "../shared/Loader/Loader";
import {Symbols} from "../../models/CurrencySymbols";
import TransactionList from "./TransactionList/TransactionList";
import NativeSelect, {SelectOnChangeFn} from "../shared/NativeSelect/NativeSelect";
import styles from "./wallets.module.scss";

export interface Props {
    wallet: Wallet,
    symbols: Symbols,
    /**
     * supports 3 digit code
     */
    selectedCurrency: string,
    showLoader?: boolean,
    onCurrencyChange: SelectOnChangeFn
}

const Wallets: FC<Props> = (props) => {
    const {
        wallet,
        symbols,
        showLoader,
        selectedCurrency,
        onCurrencyChange
    } = props;

    const options = [];
    for (const symbolKey in symbols) {
        if (Object.prototype.hasOwnProperty.call(symbols, symbolKey)) {
            options.push(
                <option key={symbolKey}>
                    {symbolKey}
                </option>
            );
        }
    }

    return (
        <React.Fragment>
            <div className={styles.walletsToolbar}>
                <Link to="/">
                    <button className={styles.btn}>
                        Exchanges
                    </button>
                </Link>
            </div>
            <section className={styles.walletsSection}>
                <div className={styles.walletsSummaryContainer}>
                    <div className={styles.transactionSummary}>
                        <div>
                            <NativeSelect
                                className={styles.currencySelect}
                                value={selectedCurrency}
                                onChange={onCurrencyChange}>
                                {options}
                            </NativeSelect>
                            <div className={styles.balanceText}>
                                {`${wallet.getBalance()} ${wallet.getWalletCurrency()}`}
                            </div>
                        </div>
                    </div>
                </div>
                <div className={styles.transactionListContainer}>
                    <TransactionList
                        selectedCurrency={selectedCurrency}
                        transactions={wallet.getSortedTransactions()}
                    />
                </div>
                {
                    showLoader ?
                        (
                            <div className={styles.loaderContainer}>
                                <Loader/>
                            </div>
                        ) : null
                }
            </section>
        </React.Fragment>
    )
};

Wallets.defaultProps = {
    showLoader: false
};

export default React.memo(Wallets);