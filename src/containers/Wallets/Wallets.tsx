import React, {
    FC,
    useState,
    useEffect,
    useCallback,
    ChangeEvent
} from "react";
import {createSelector} from "reselect"
import {useDispatch, useSelector} from "react-redux";
import {RootState} from "../../reducers";
import Wallet from "../../models/Wallet";
import WalletModel from "../../models/Wallets";
import {WalletStore} from "../../reducers/wallet";
import {fetchWallets} from "../../actions/wallet";
import WalletsView from "../../components/Wallets/Wallets";
import {fetchSymbolsIfNeeded} from "../../actions/currencySymbols";
import {CurrencySymbolsStore} from "../../reducers/currencySymbols";

const selectSymbols = createSelector<RootState, CurrencySymbolsStore, CurrencySymbolsStore>(
    state => state.currencySymbols,
    currencySymbols => currencySymbols || {}
);

const walletsSelector = createSelector<RootState, WalletStore, WalletModel>(
    state => state.wallets,
    wallets => new WalletModel(wallets.wallets, null)
);

const Wallets: FC = () => {

    const [selectedCurrency, setSelectedCurrency] = useState<string>("");

    const dispatch = useDispatch();

    const symbols = useSelector(selectSymbols);
    const wallets = useSelector(walletsSelector);

    // effect to fetch symbols and wallets api
    useEffect(() => {
        dispatch(fetchSymbolsIfNeeded());
        dispatch(fetchWallets());
    }, [dispatch]);

    // effect to set default selected currencies
    useEffect(() => {
        if (symbols.symbols) {
            const symbols3Letter = Object.keys(symbols.symbols);
            if (symbols3Letter.length > 1) {
                setSelectedCurrency(symbols3Letter[0]);
            }
        }
    }, [symbols]);


    const memorizeHandleOnCurrencyChange = useCallback(
        (event: ChangeEvent<HTMLSelectElement>, value: string) => {
            setSelectedCurrency(value);
        }, []);

    let wallet = new Wallet({
        currency: selectedCurrency,
        balance: 0,
        transactions: []
    });

    if (wallets.has(selectedCurrency)) {
        wallet = wallets.get(selectedCurrency) || wallet;
    }

    return (
        <WalletsView
            wallet={wallet}
            symbols={symbols.symbols || {}}
            showLoader={symbols.isFetching}
            selectedCurrency={selectedCurrency}
            onCurrencyChange={memorizeHandleOnCurrencyChange}
        />
    );
};

export default Wallets;