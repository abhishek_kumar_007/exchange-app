import {
    RECEIVE_WALLETS, SAVE_WALLET,
    WalletsAction
} from "../actions/wallet";
import {StoreWithFetch} from "../types";
import {WalletsResponse} from "../communicators/Wallet";

export interface WalletStore extends StoreWithFetch {
    wallets: WalletsResponse | null
}

export default function wallets(
    state: WalletStore = {
        isFetching: false,
        didInvalidate: false,
        lastUpdated: null,
        wallets: null,
    },
    action: WalletsAction
): WalletStore {
    switch (action.type) {
        case SAVE_WALLET:

            let wallets = {
                ...state.wallets,
                ...action.updatedWallets
            };

            return {
                ...state,
                wallets
            };
        case RECEIVE_WALLETS:
            return {
                ...state,
                isFetching: false,
                didInvalidate: false,
                wallets: action.wallets || null,
                lastUpdated: action.receivedAt
            };
        default:
            return state
    }
}
