import React from "react";
import thunk from "redux-thunk";
import fetchMock from "fetch-mock";
import {Provider} from "react-redux";
import Enzyme, {mount} from "enzyme";
import Adapter from "enzyme-adapter-react-16";
import configureMockStore from "redux-mock-store"
import {HashRouter as Router} from "react-router-dom";
import Wallets from "./Wallets";
import {RootState} from "../../reducers";
import WalletsModel from "../../models/Wallets";
import WalletCommunicators from "../../communicators/Wallet";

const walletCommunicator = new WalletCommunicators();

Enzyme.configure({adapter: new Adapter()});

const mockStore = configureMockStore<RootState>([thunk]);

describe("Exchange App", () => {

    afterEach(() => {
        fetchMock.restore()
    });

    it("Should render Wallets component", () => {

        const mockWalletData = walletCommunicator.getMockWalletData();

        const store = mockStore({
            currencySymbols: {symbols: {"USD": "USD", "INR": "INR"}, isFetching: false, didInvalidate: false},
            currenciesRate: {isFetching: false, currenciesRate: null, didInvalidate: false},
            wallets: {wallets: mockWalletData, isFetching: false, didInvalidate: false}
        });

        const wrapper = mount(
            <Provider store={store}>
                <Router>
                    <Wallets/>
                </Router>
            </Provider>
        );

        const selectedCurrency = wrapper.childAt(0).childAt(0).childAt(0).childAt(0).prop("selectedCurrency");
        expect(selectedCurrency).toEqual("USD");


        expect(
            wrapper.childAt(0)
                .childAt(0)
                .childAt(0)
                .childAt(0)
                .prop("wallet"))
            .toEqual(new WalletsModel(mockWalletData, null).get(selectedCurrency));
    })
});