import reducer from "./wallet";
import {saveWalletAction} from "../actions/wallet";
import WalletCommunicators from "../communicators/Wallet";

const walletCommunicator = new WalletCommunicators();

describe("Wallets reducer", () => {

    it("should return the initial state", () => {
        expect(reducer(undefined, {type: ""})).toEqual(
            {
                isFetching: false,
                didInvalidate: false,
                lastUpdated: null,
                wallets: null
            }
        )
    });

    it("should save wallets", () => {
        expect(
            reducer(
                undefined,
                saveWalletAction(walletCommunicator.getMockWalletData())
            )
        ).toEqual(
            {
                didInvalidate: false,
                isFetching: false,
                lastUpdated: null,
                wallets: walletCommunicator.getMockWalletData()
            }
        )
    })
});