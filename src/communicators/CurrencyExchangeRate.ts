/**
 * CurrencyExchangeRate Communicator to fetch currency exchange value.
 */

import BaseCommunicator, {BaseURL} from "./BaseCommunicator";
import {CurrenciesRateMap} from "../models/CurrenciesRate";

const url: BaseURL = process.env.REACT_APP_EXCHANGE_GENERATE_BASE_URL;

export interface FetchCurrenciesRateOptions {
    baseCurrency: string
}

export interface CurrencyExchangeRateResponse {
    time_last_updated: string;
    base: string;
    date: string;
    rates: CurrenciesRateMap;
}

class CurrencyExchangeRate extends BaseCommunicator {
    constructor() {
        super(url);
    }

    /**
     * Fetches Latest Currency Rate for given baseCurrency
     *
     * @note - Using a free API, result might not be upto date.
     * @param baseCurrency
     */
    getLatestCurrenciesValue = async (
        {baseCurrency}: FetchCurrenciesRateOptions
    ): Promise<CurrencyExchangeRateResponse | null> => {
        const result = await this.get(`/latest/${baseCurrency}`, null);
        if (result && result.rates) {
            return result;
        }
        return null;
    };
}

export default CurrencyExchangeRate;