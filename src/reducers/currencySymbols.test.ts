import reducer from "./currencySymbols";
import {
    receiveSymbols,
    requestSymbols
} from "../actions/currencySymbols";

describe("Currency Symbol reducer", () => {

    it("should return the initial state", () => {
        expect(reducer(undefined, {type: ""})).toEqual(
            {
                isFetching: false,
                didInvalidate: false,
                lastUpdated: null,
                symbols: null
            }
        )
    });

    it("should request symbols", () => {
        expect(
            reducer(undefined, requestSymbols())
        ).toEqual({
                isFetching: true,
                didInvalidate: false,
                lastUpdated: null,
                symbols: null
            }
        );
    });

    it("should save symbols", () => {
        expect(
            reducer(
                undefined,
                receiveSymbols({"INR": "INR"})
            )
        ).toEqual(
            {
                didInvalidate: false,
                isFetching: false,
                symbols: {
                    "INR": "INR"
                }
            }
        )
    })
});