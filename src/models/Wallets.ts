import Wallet from "./Wallet";
import {WalletsResponse} from "../communicators/Wallet";

class Wallets extends Map<string, Wallet> {

    constructor(walletObject: WalletsResponse | null, wallets: Wallets | null) {
        super(wallets || new Map());
        if (walletObject) {
            for (let currency in walletObject) {
                if (Object.prototype.hasOwnProperty.call(walletObject, currency)) {
                    this.set(currency, new Wallet(walletObject[currency]));
                }
            }
        }
    }

    getWalletsObject(): WalletsResponse {
        const wallets: WalletsResponse = {};
        this.forEach((value, key) => {
            wallets[key] = value.getWallet();
        });

        return wallets;
    }
}

export default Wallets;