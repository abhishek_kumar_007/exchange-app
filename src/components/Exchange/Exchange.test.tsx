import React from "react";
import Enzyme, {mount} from "enzyme";
import Adapter from "enzyme-adapter-react-16";
import {HashRouter as Router} from "react-router-dom";
import {render, fireEvent} from "@testing-library/react";
import Wallets from "../../models/Wallets";
import Exchange, {OnExchangeFnArgs} from "./Exchange";
import CurrenciesRate from "../../models/CurrenciesRate";
import WalletCommunicators from "../../communicators/Wallet";

Enzyme.configure({adapter: new Adapter()});

const walletCommunicator = new WalletCommunicators();

describe("Exchange Component", () => {

    it("Check for currency exchange", (doneCallback) => {

        const {getAllByTestId, getByTestId} = render(
            <Router>
                <Exchange
                    wallets={new Wallets(walletCommunicator.getMockWalletData(), null)}
                    onExchange={(args: OnExchangeFnArgs) => {
                        expect(args).toEqual({
                            currencyTo: "INR",
                            currencyFrom: "USD",
                            valueFrom: 5,
                            valueTo: 350
                        });

                        doneCallback();
                    }}
                    symbols={{"USD": "USD", "INR": "INR"}}
                    selectedCurrencyFrom="USD"
                    selectedCurrencyTo="INR"
                    currenciesRate={new CurrenciesRate({date: "", timestamp: "", base: "USD", rates: {"INR": 70}})}
                />
            </Router>
        );

        const node = getAllByTestId("input");
        // setting value to 5 USD
        fireEvent.change(node[0], {target: {value: 5}});

        // @ts-ignore
        // value should be 350 INR
        expect(node[1].value).toEqual("350");

        const exchangeBtnNode = getByTestId("btn-exchange");
        if (exchangeBtnNode) {
            // clicking on exchange btn
            fireEvent.click(exchangeBtnNode);
        }
    });

    it("Check for exchange info", () => {

        const wrapper = mount(
            <Router>
                <Exchange
                    wallets={new Wallets({}, null)}
                    onExchange={() => {
                    }}
                    symbols={{"USD": "USD", "INR": "INR"}}
                    selectedCurrencyFrom="USD"
                    selectedCurrencyTo="INR"
                    currenciesRate={new CurrenciesRate({date: "", timestamp: "", base: "USD", rates: {"INR": 70}})}
                />
            </Router>
        );

        const nodes = wrapper.find("Pocket");
        expect(nodes.at(0).prop("exchangeRateInfo")).toEqual("1 USD = 70 INR");
        expect(nodes.at(1).prop("exchangeRateInfo")).toEqual("1 INR = 0.01 USD");
    })
});