import React, {ChangeEvent, useCallback} from "react";

export type SelectOnChangeFn = (event: ChangeEvent<HTMLSelectElement>, value: string) => void;

export interface Props {
    value?: string,
    style?: Object,
    className?: string,
    children: React.ReactNode,
    onChange?: SelectOnChangeFn
}

function NativeSelect(this: any, props: Props) {

    const {className, style, onChange, children, ...other} = props;

    const memorizedSelectOnChange = useCallback((event: ChangeEvent<HTMLSelectElement>) => {
        if (typeof onChange === "function") {
            return onChange(event, event.currentTarget.value || "")
        }
    }, [onChange]);

    return (
        <select
            className={className}
            style={style}
            onChange={memorizedSelectOnChange}
            {...other}>
            {children}
        </select>
    );
}

NativeSelect.defaultProps = {
    onChange: () => null,
    value: "",
    style: null,
    className: ""
};

export default NativeSelect;
