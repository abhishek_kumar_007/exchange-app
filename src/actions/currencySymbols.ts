import {Dispatch} from "react";
import {RootState} from "../reducers";
import {BaseReduxAction} from "../types";
import {Symbols} from "../models/CurrencySymbols";
import CurrencySymbols from "../communicators/CurrencySymbols";

export interface CurrencySymbolsAction extends BaseReduxAction {
    symbols?: Symbols
}

export const REQUEST_SYMBOLS = "REQUEST_SYMBOLS";
export const RECEIVE_SYMBOLS = "RECEIVE_SYMBOLS";
export const INVALIDATE_SYMBOLS = "INVALIDATE_SYMBOLS";

const currencySymbolsCommunicator = new CurrencySymbols();

export function requestSymbols(): CurrencySymbolsAction {
    return {
        type: REQUEST_SYMBOLS
    }
}

export function receiveSymbols(symbols: Symbols): CurrencySymbolsAction {
    return {
        type: RECEIVE_SYMBOLS,
        symbols,
    }
}

/**
 * Method to fetch currency/symbols via API
 */
export function fetchSymbols() {
    return async (dispatch: Dispatch<Object>) => {
        dispatch(requestSymbols());
        const response = await currencySymbolsCommunicator.getCurrencySymbols();

        const symbols = response.getSymbols();

        /**
         * Symbols API contains currency which were not supported by the Wallets API.
         * So for now I have filtered out the unsupported currency/symbols in Wallets API.
         *
         * We can remove this once we have an API which supports all. Most of them were paid ones.
         */

        const allowedCurrencyViaApi = ["USD", "EUR", "GBP", "INR"];
        const supportedSymbols: Symbols = {};

        for (let symbolsKey in symbols) {
            if (Object.prototype.hasOwnProperty.call(symbols, symbolsKey) &&
                allowedCurrencyViaApi.includes(symbolsKey)) {
                supportedSymbols[symbolsKey] = symbols[symbolsKey];
            }
        }
        dispatch(receiveSymbols(supportedSymbols));
    }
}

function shouldFetchSymbols(store: RootState) {
    const {symbols} = store.currencySymbols;
    if (!symbols) {
        return true;
    } else if (symbols && symbols.isFetching) {
        return false;
    } else {
        return symbols.didInvalidate;
    }
}

export function fetchSymbolsIfNeeded() {
    return (dispatch: Dispatch<Object>, getStore: Function) => {
        if (shouldFetchSymbols(getStore())) {
            return dispatch(fetchSymbols())
        }
    }
}