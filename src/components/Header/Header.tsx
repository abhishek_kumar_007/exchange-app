import React from "react";
import styles from "./header.module.scss";

function Header() {
    return (
        <div className={styles.header}>
            <div className={styles.headerContainer}>
                <div className={styles.title}>Exchange App</div>
            </div>
        </div>
    );
}

export default Header;