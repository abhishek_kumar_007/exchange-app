import {
    REQUEST_SYMBOLS,
    RECEIVE_SYMBOLS,
    INVALIDATE_SYMBOLS,
    CurrencySymbolsAction
} from "../actions/currencySymbols";
import {StoreWithFetch} from "../types";
import {Symbols} from "../models/CurrencySymbols";

export interface CurrencySymbolsStore extends StoreWithFetch {
    symbols: Symbols | null
}

export default function currencySymbols(
    state: CurrencySymbolsStore = {
        isFetching: false,
        didInvalidate: false,
        lastUpdated: null,
        symbols: null
    },
    action: CurrencySymbolsAction
): CurrencySymbolsStore {
    switch (action.type) {
        case INVALIDATE_SYMBOLS:
            return {...state, ...{didInvalidate: true}};
        case REQUEST_SYMBOLS:
            return {
                ...state,
                ...{
                    isFetching: true,
                    didInvalidate: false
                }
            };
        case RECEIVE_SYMBOLS:
            return {
                ...state,
                isFetching: false,
                didInvalidate: false,
                symbols: action.symbols || null,
                lastUpdated: action.receivedAt
            };
        default:
            return state
    }
}
