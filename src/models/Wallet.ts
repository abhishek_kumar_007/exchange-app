export interface WalletTransaction {
    transactionId: string;
    amount: number;
    currency: string,
    exchangedAmount: number;
    exchangedCurrency: string;
    timestamp: number;
}

export interface WalletOptions {
    currency: string;
    balance: number;
    transactions: Array<WalletTransaction>;
}

class Wallet {
    _currency: string;
    _balance: number;
    _transactions: Array<WalletTransaction>;

    constructor({currency, balance, transactions}: WalletOptions) {
        this._currency = currency;
        this._balance = balance || 0;
        this._transactions = [...transactions] || [];
    }

    getWalletCurrency(): string {
        return this._currency;
    }

    getTransactions(): Array<WalletTransaction> {
        return Array.isArray(this._transactions) ? this._transactions : [];
    }

    getSortedTransactions(): Array<WalletTransaction> {
        return this.getTransactions().reverse();
    }

    setTransaction(transaction: WalletTransaction) {
        if (Array.isArray(this._transactions)) {
            this._transactions.push(transaction);
        }
    }

    getBalance(): number {
        return this._balance;
    }

    setBalance(balance: number) {
        this._balance = balance;
    }

    getWallet() {
        return {
            currency: this._currency,
            balance: this._balance,
            transactions: [...this._transactions]
        }
    }

    getGeneratedTransactionId(): string {
        return this._currency + "-" + (Array.isArray(this._transactions) ? this._transactions.length : 0);
    }
}

export default Wallet;