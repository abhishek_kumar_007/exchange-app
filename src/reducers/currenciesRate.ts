import {
    RECEIVE_CURRENCIES_RATE,
    REQUEST_CURRENCIES_RATE,
    INVALIDATE_CURRENCY_RATE,
    CurrenciesRateAction
} from "../actions/currenciesRate";
import {StoreWithFetch} from "../types";
import {CurrenciesRateOptions} from "../models/CurrenciesRate";

export interface CurrenciesRateStore extends StoreWithFetch {
    currenciesRate: CurrenciesRateOptions | null
}

export default function currencySymbols(
    state: CurrenciesRateStore = {
        isFetching: false,
        didInvalidate: false,
        lastUpdated: null,
        currenciesRate: null
    },
    action: CurrenciesRateAction
): CurrenciesRateStore {
    switch (action.type) {
        case INVALIDATE_CURRENCY_RATE:
            return {...state, ...{didInvalidate: true}};
        case REQUEST_CURRENCIES_RATE:
            return {
                ...state,
                ...{
                    isFetching: true,
                    didInvalidate: false
                }
            };
        case RECEIVE_CURRENCIES_RATE:
            return {
                ...state,
                isFetching: false,
                didInvalidate: false,
                currenciesRate: action.currenciesRate || null,
                lastUpdated: action.receivedAt
            };
        default:
            return state
    }
}
