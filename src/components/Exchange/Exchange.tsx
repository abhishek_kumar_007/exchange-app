import React, {ChangeEvent, FC, SyntheticEvent, useCallback, useEffect, useState} from "react";
import classNames from "classnames";
import {Link} from "react-router-dom";
import Pocket from "./Pocket/Pocket";
import Wallet from "../../models/Wallet";
import Wallets from "../../models/Wallets";
import Loader from "../shared/Loader/Loader";
import {Symbols} from "../../models/CurrencySymbols";
import CurrenciesRate from "../../models/CurrenciesRate";
import {NumberInput} from "../shared/NumberField/NumberField";
import {SelectOnChangeFn} from "../shared/NativeSelect/NativeSelect";
import styles from "./exchange.module.scss";

export interface OnExchangeFnArgs {
    currencyFrom: string;
    currencyTo: string;
    valueFrom: number;
    valueTo: number;
}

export type OnExchangeFn = (args: OnExchangeFnArgs) => void;

export interface Props {
    wallets: Wallets,
    symbols: Symbols,
    /**
     * supports 3 digit code
     */
    selectedCurrencyFrom: string,
    /**
     * supports 3 digit code
     */
    selectedCurrencyTo: string,
    showLoader?: boolean,
    currenciesRate: CurrenciesRate | null,
    onExchange: OnExchangeFn,
    onCurrencyChangeTo?: SelectOnChangeFn,
    onCurrencyChangeFrom?: SelectOnChangeFn,
}

export interface CurrencyValueState {
    from: NumberInput,
    to: NumberInput
}

const Exchange: FC<Props> = (props) => {
    const {
        symbols,
        wallets,
        showLoader,
        currenciesRate,
        onExchange,
        selectedCurrencyFrom,
        selectedCurrencyTo,
        onCurrencyChangeFrom,
        onCurrencyChangeTo
    } = props;

    const [currencyValue, setCurrencyValue] = useState<CurrencyValueState>({from: 0, to: 0});

    // values based on 1 unit
    const currencyBaseValue = currenciesRate?.getRate(selectedCurrencyTo) ?? 0;
    const baseCurrencyValue = currenciesRate?.convertToBaseCurrencyRate(selectedCurrencyFrom, selectedCurrencyTo) ?? 0;

    let fromWallet = new Wallet({
        currency: selectedCurrencyFrom,
        balance: 0,
        transactions: []
    });

    let toWallet = new Wallet({
        currency: selectedCurrencyTo,
        balance: 0,
        transactions: []
    });

    if (wallets.has(selectedCurrencyFrom)) {
        fromWallet = wallets.get(selectedCurrencyFrom) || fromWallet;
    }

    if (wallets.has(selectedCurrencyTo)) {
        toWallet = wallets.get(selectedCurrencyTo) || toWallet;
    }

    useEffect(() => {
        if (selectedCurrencyFrom && selectedCurrencyTo) {

            const balance = wallets.get(selectedCurrencyFrom)?.getBalance() ?? 0;

            setCurrencyValue((state) => {
                const fromValue = state.from > balance ? 0 : state.from;
                return {
                    ...state,
                    from: fromValue,
                    to: currenciesRate?.convertFromBaseCurrencyRate(
                        selectedCurrencyFrom,
                        selectedCurrencyTo,
                        fromValue || 0
                    ) ?? 0
                }
            });
        }
    }, [currenciesRate, selectedCurrencyFrom, selectedCurrencyTo, wallets]);

    // callback handler for onCurrencyValue change
    const memoHandleOnCurrencyValueChange = useCallback(
        (fieldToUpdate: string) => {
            return (event: ChangeEvent<HTMLInputElement>, value: NumberInput) => {
                let fromValue: NumberInput = 0;
                let toValue: NumberInput = 0;

                setCurrencyValue((state) => {
                    if (fieldToUpdate === "from") {
                        fromValue = value;
                        toValue = currenciesRate?.convertFromBaseCurrencyRate(
                            selectedCurrencyFrom,
                            selectedCurrencyTo,
                            value || 0
                        ) ?? 0;
                    } else {
                        toValue = value;
                        fromValue = currenciesRate?.convertToBaseCurrencyRate(
                            selectedCurrencyFrom,
                            selectedCurrencyTo,
                            value || 0
                        ) ?? 0;

                        if (fromValue > fromWallet.getBalance()) {
                            fromValue = state.from;
                            toValue = state.to;
                        }
                    }

                    return {
                        ...state,
                        from: fromValue,
                        to: toValue
                    }
                });
            }
        }, [selectedCurrencyFrom, currenciesRate, selectedCurrencyTo, fromWallet]
    );

    const memoOnExchangeClicked = useCallback((event: SyntheticEvent<HTMLButtonElement>) => {
        if (typeof onExchange === "function") {
            const fromWalletBalance = fromWallet.getBalance();
            if (currencyValue.from <= fromWalletBalance) {
                const exchangedValue = currenciesRate?.convertFromBaseCurrencyRate(
                    selectedCurrencyFrom,
                    selectedCurrencyTo,
                    currencyValue.from || 0
                ) ?? 0;

                setCurrencyValue({
                    from: 0,
                    to: 0
                });

                onExchange({
                    currencyFrom: selectedCurrencyFrom,
                    currencyTo: selectedCurrencyTo,
                    valueFrom: currencyValue.from || 0,
                    valueTo: exchangedValue
                });
            }
        }
    }, [
        onExchange,
        fromWallet,
        currencyValue,
        currenciesRate,
        selectedCurrencyTo,
        selectedCurrencyFrom
    ]);

    return (
        <React.Fragment>
            <div className={styles.exchangeToolbar}>
                <Link to="/wallets">
                    <button className={classNames(styles.btn, styles.walletBtn)}>
                        Wallet
                    </button>
                </Link>
                <button
                    data-testid="btn-exchange"
                    disabled={(selectedCurrencyTo === selectedCurrencyFrom) || currencyValue.from <= 0}
                    onClick={memoOnExchangeClicked}
                    className={styles.btn}
                >
                    Exchange
                </button>
            </div>
            <section className={styles.exchangeSection}>
                <Pocket
                    symbols={symbols}
                    wallet={fromWallet}
                    inputProps={{
                        max: fromWallet.getBalance()
                    }}
                    showExchangeFlowIcon
                    currencyValue={currencyValue.from}
                    selectedCurrency={selectedCurrencyFrom}
                    onCurrencyChange={onCurrencyChangeFrom}
                    className={styles.pocketContainerFrom}
                    onCurrencyValueChange={memoHandleOnCurrencyValueChange("from")}
                    exchangeRateInfo={`1 ${selectedCurrencyFrom} = ${currencyBaseValue} ${selectedCurrencyTo}`}
                />
                <Pocket
                    symbols={symbols}
                    wallet={toWallet}
                    inputProps={{
                        min: 1
                    }}
                    currencyValue={currencyValue.to}
                    selectedCurrency={selectedCurrencyTo}
                    onCurrencyChange={onCurrencyChangeTo}
                    className={styles.pocketContainerTo}
                    onCurrencyValueChange={memoHandleOnCurrencyValueChange("to")}
                    exchangeRateInfo={`1 ${selectedCurrencyTo} = ${baseCurrencyValue} ${selectedCurrencyFrom}`}
                />
                {
                    showLoader ?
                        (
                            <div className={styles.loaderContainer}>
                                <Loader/>
                            </div>
                        ) : null
                }
            </section>
        </React.Fragment>
    )
};

Exchange.defaultProps = {
    showLoader: false
};

export default React.memo(Exchange);