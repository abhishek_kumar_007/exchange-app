/**
 * CurrencySymbols Communicator to fetch all symbols data
 */

import BaseCommunicator, {BaseURL} from "./BaseCommunicator";
import CurrencySymbolsModel from "../models/CurrencySymbols";

const url: BaseURL = process.env.REACT_APP_FIXER_BASE_URL;
const accessKey = process.env.REACT_APP_FIXER_ACCESS_KEY;

export interface FetchCurrenciesRateOptions {
    baseCurrency: string
}

class CurrencySymbols extends BaseCommunicator {
    constructor() {
        super(url);
    }

    /**
     * Fetches all currency symbols
     */
    getCurrencySymbols = async (): Promise<CurrencySymbolsModel> => {
        if (accessKey) {
            const urlSearchParams = new URLSearchParams();
            urlSearchParams.append("access_key", accessKey);
            const result = await this.get("/symbols", urlSearchParams, {mode: "cors", redirect: "follow"});
            let symbols = {};
            if (result && result.success === true && result.symbols) {
                symbols = result.symbols;
            }

            return new CurrencySymbolsModel(symbols);
        }

        throw new Error("CurrencySymbols::getCurrencySymbols:: access key not defined")
    };
}

export default CurrencySymbols;