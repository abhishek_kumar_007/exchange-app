import React from "react";
import thunk from "redux-thunk";
import fetchMock from "fetch-mock";
import {Provider} from "react-redux";
import Enzyme, {mount} from "enzyme";
import Adapter from "enzyme-adapter-react-16";
import configureMockStore from "redux-mock-store"
import {HashRouter as Router} from "react-router-dom";
import Exchange from "./Exchange";
import {RootState} from "../../reducers";
import BaseCommunicator, {BaseURL} from "../../communicators/BaseCommunicator";

Enzyme.configure({adapter: new Adapter()});

const mockStore = configureMockStore<RootState>([thunk]);
const baseUrl: BaseURL = process.env.REACT_APP_FIXER_BASE_URL;
const accessKey = process.env.REACT_APP_FIXER_ACCESS_KEY;
const exchangeBaseUrl: BaseURL = process.env.REACT_APP_EXCHANGE_GENERATE_BASE_URL;

describe("Exchange App", () => {

    afterEach(() => {
        fetchMock.restore()
    });

    it("Should render Exchange component", () => {

        const symbolsUrl = BaseCommunicator.resolveURL(baseUrl, "/symbols");
        const exchangeUrl = BaseCommunicator.resolveURL(exchangeBaseUrl, "/latest/USD");

        const params = new URLSearchParams();
        params.append("access_key", String(accessKey));

        const symbolsUrlStr = BaseCommunicator.urlWithSearchParams(symbolsUrl, params);

        // mocking symbols API
        fetchMock.getOnce(symbolsUrlStr, {
            body: {success: true, symbols: {"USD": "USD", "INR": "INR"}},
            headers: {"content-type": "application/json"}
        });

        // mocking exchange rate API
        fetchMock.getOnce(exchangeUrl.toJSON(), {
            body: {base: "USD", rates: {"INR": 70.00}},
            headers: {"content-type": "application/json"}
        });

        const store = mockStore({
            currencySymbols: {symbols: {"USD": "USD", "INR": "INR"}, isFetching: false, didInvalidate: false},
            currenciesRate: {isFetching: false, currenciesRate: null, didInvalidate: false},
            wallets: {wallets: null, isFetching: false, didInvalidate: false}
        });

        const wrapper = mount(
            <Provider store={store}>
                <Router>
                    <Exchange/>
                </Router>
            </Provider>
        );

        expect(wrapper.childAt(0).childAt(0).childAt(0).childAt(0).prop("symbols"))
            .toEqual({
                "USD": "USD",
                "INR": "INR"
            });
        expect(wrapper.childAt(0).childAt(0).childAt(0).childAt(0).prop("selectedCurrencyFrom"))
            .toEqual("USD");

        expect(wrapper.childAt(0).childAt(0).childAt(0).childAt(0).prop("selectedCurrencyTo"))
            .toEqual("INR");
    })
});