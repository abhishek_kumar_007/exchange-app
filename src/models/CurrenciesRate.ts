export type CurrenciesRateMap = { [key: string]: number };

export interface CurrenciesRateOptions {
    timestamp: string;
    base: string;
    date: string;
    rates: CurrenciesRateMap;
}

class CurrenciesRate {
    _timestamp: string;
    _base: string;
    _date: string;
    _rates: CurrenciesRateMap;

    constructor({timestamp, base, date, rates}: CurrenciesRateOptions) {
        this._timestamp = timestamp;
        this._base = base;
        this._date = date;
        this._rates = rates;
    }

    /**
     * Method that return rate for the given currency param for 1 unit of baseCurrency
     * @param currency
     */
    getRate(currency: string) {
        if (Object.prototype.hasOwnProperty.call(this._rates, currency)) {
            return Number(parseFloat(String(this._rates[currency])).toFixed(2));
        }
    };

    /**
     * Method that converts from base currency value to the given currency value.
     *
     * @param baseCurrency
     * @param toCurrency
     * @param value
     */
    convertFromBaseCurrencyRate(baseCurrency: string, toCurrency: string, value: number): number {
        if (baseCurrency === this._base) {
            if (Object.prototype.hasOwnProperty.call(this._rates, toCurrency)) {
                const rawRate = this._rates[toCurrency];
                return Number(parseFloat(String(value * rawRate)).toFixed(2));
            }
        }

        return 0;
    }

    /**
     * Method that converts currency value to base currency value.
     *
     * @param baseCurrency
     * @param currency
     * @param value
     */
    convertToBaseCurrencyRate(baseCurrency: string, currency: string, value: number = 1): number {
        if (baseCurrency === this._base) {
            if (Object.prototype.hasOwnProperty.call(this._rates, currency)) {
                const rawRate = this._rates[currency];

                return Number(parseFloat(String((1 / rawRate) * value)).toFixed(2));
            }
        }
        return 0;
    }
}

export default CurrenciesRate;