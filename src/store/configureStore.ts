import {configureStore} from "@reduxjs/toolkit"
import rootReducer from "../reducers"

function getStore() {
    const store = configureStore({
        reducer: rootReducer()
    });

    // @ts-ignore
    if (process.env.NODE_ENV === "development" && module.hot) {
        // @ts-ignore
        module.hot.accept("../reducers", () => {
            const newRootReducer = require("../reducers").default;
            store.replaceReducer(newRootReducer())
        })
    }

    return store;
}

export default getStore;