import React, {ChangeEvent, FC, KeyboardEvent, useCallback} from "react";

export type NumberInput = number | "";
export type NumberFieldOnChangeFn = (event: ChangeEvent<HTMLInputElement>, value: NumberInput) => void;

export interface Props {
    /**
     * Gets fired when value gets changed
     * @param value
     */
    onChange?: NumberFieldOnChangeFn,
    disabled?: boolean,
    style?: Object,
    className?: string,
    value?: number | "",
    placeholder?: string,
    /**
     * @default 1000
     */
    max?: number,
    /**
     * @default -1000
     */
    min?: number,
    /**
     * @default 1
     */
    step?: number
}

const NumberField: FC<Props> = (props: Props) => {

    const {
        className,
        style,
        max = 100000,
        min = -100000,
        step,
        value,
        disabled,
        placeholder,
        onChange
    } = props;

    const memorizedNumberOnChange = useCallback((event: ChangeEvent<HTMLInputElement>) => {
        if (typeof onChange === "function") {
            if (event.currentTarget.value !== "") {
                const value = Number(event.currentTarget.value);
                if (value <= max && value >= min) {
                    return onChange(event, Number(event.currentTarget.value))
                }
            } else {
                return onChange(event, "")
            }
        }
    }, [onChange, max, min]);

    const memoOnKeyPress = useCallback((event: KeyboardEvent<HTMLInputElement>) => {
        if (event.which !== 46) {
            const value = String(event.currentTarget.value);
            if (!/^\d+(\.\d{1,2})?$/g.test(value + event.key)) {
                event.stopPropagation();
                event.preventDefault();
            }
        }
        return true;
    }, []);

    return (
        <input
            data-testid="input"
            className={className}
            style={style}
            type="number"
            max={max}
            min={min}
            step={step}
            value={value}
            placeholder={placeholder}
            onChange={memorizedNumberOnChange}
            disabled={disabled}
            onKeyPress={memoOnKeyPress}
        />
    );
};

NumberField.defaultProps = {
    step: 1,
    value: "",
    style: {},
    className: "",
    max: 1000000000,
    min: -1000000000,
    disabled: false,
    placeholder: "",
    onChange: () => null
};

export default React.memo(NumberField);
