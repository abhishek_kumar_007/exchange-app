import React from "react";
import classNames from "classnames";
import styles from "./loader.module.scss";

interface Props {
    /**
     * className for container
     */
    className?: string
}

function Loader({className}: Props) {
    return (
        <div className={classNames(styles.ripple, className)}>
            <div/>
            <div/>
        </div>
    )
}

export default Loader;