import React, {
    ChangeEvent,
    Dispatch, FC,
    SetStateAction,
    useCallback,
    useEffect,
    useState
} from "react";
import {createSelector} from "reselect"
import {useDispatch, useSelector} from "react-redux";
import {RootState} from "../../reducers";
import Wallet from "../../models/Wallet";
import Wallets from "../../models/Wallets";
import Constants from "../../utils/Constants";
import {WalletStore} from "../../reducers/wallet";
import CurrenciesRate from "../../models/CurrenciesRate";
import {fetchWallets, saveWallet} from "../../actions/wallet";
import {fetchCurrenciesRate} from "../../actions/currenciesRate";
import {CurrenciesRateStore} from "../../reducers/currenciesRate";
import {fetchSymbolsIfNeeded} from "../../actions/currencySymbols";
import {CurrencySymbolsStore} from "../../reducers/currencySymbols";
import ExchangeView, {OnExchangeFnArgs} from "../../components/Exchange/Exchange";

export interface SelectedCurrencyState {
    from: string,
    to: string
}

const selectSymbols = createSelector<RootState, CurrencySymbolsStore, CurrencySymbolsStore>(
    state => state.currencySymbols,
    currencySymbols => currencySymbols
);

const selectCurrenciesRate = createSelector<RootState, CurrenciesRateStore, CurrenciesRate | null>(
    state => state.currenciesRate,
    currencyRates => currencyRates.currenciesRate ? new CurrenciesRate(currencyRates.currenciesRate) : null
);

const walletsSelector = createSelector<RootState, WalletStore, Wallets>(
    state => state.wallets,
    wallets => new Wallets(wallets.wallets, null)
);

function handleOnCurrencyChange(
    dispatch: Dispatch<SetStateAction<SelectedCurrencyState>>,
    fieldToUpdate: string,
    value: string
) {
    dispatch((state) => {
        return {
            ...state,
            [fieldToUpdate]: value
        }
    });
}

const Exchange: FC = () => {

    const [selectedCurrency, setSelectedCurrency] = useState<SelectedCurrencyState>({from: "", to: ""});

    const dispatch = useDispatch();

    const symbols = useSelector(selectSymbols);
    const wallets = useSelector(walletsSelector);
    const currenciesRate = useSelector(selectCurrenciesRate);

    // effect to dispatch symbols api and wallets
    useEffect(() => {
        dispatch(fetchSymbolsIfNeeded());
         dispatch(fetchWallets());
    }, [dispatch]);

    // effect to set default selected currencies
    useEffect(() => {
        if (symbols && symbols.symbols) {
            const symbols3Letter = Object.keys(symbols.symbols);
            if (symbols3Letter.length > 1) {
                setSelectedCurrency({
                    from: symbols3Letter[0],
                    to: symbols3Letter[1]
                });
            }
        }
    }, [symbols]);

    //effect to fetch exchange rate based on selected currency
    useEffect(() => {
        if (selectedCurrency.from !== "") {

            dispatch(fetchCurrenciesRate({
                baseCurrency: selectedCurrency.from
            }));

            const timeoutID = setInterval(() => {
                dispatch(fetchCurrenciesRate({
                    baseCurrency: selectedCurrency.from
                }))
            }, Constants.EXCHANGE_POLL_INTERVAL);

            // gets called when effect gets destroyed
            return () => {
                clearInterval(timeoutID);
            }
        }
    }, [dispatch, selectedCurrency.from]);

    const memorizeHandleOnCurrencyChange = useCallback((fieldToUpdate: string) => {
        return (event: ChangeEvent<HTMLSelectElement>, value: string) => {
            handleOnCurrencyChange(setSelectedCurrency, fieldToUpdate, value);
        }
    }, []);

    const memoOnExchange = useCallback((args: OnExchangeFnArgs) => {

        const walletsClone = new Wallets({}, wallets);

        if (walletsClone.has(args.currencyFrom) && walletsClone.has(args.currencyTo)) {

            const fromWallet = walletsClone.get(args.currencyFrom) || new Wallet({
                currency: args.currencyFrom,
                balance: 0,
                transactions: []
            });

            const toWallet = walletsClone.get(args.currencyTo) || new Wallet({
                currency: args.currencyTo,
                balance: 0,
                transactions: []
            });

            const fromWalletBalance = fromWallet.getBalance() || 0;
            const toWalletBalance = toWallet.getBalance() || 0;

            if (fromWalletBalance > 0 && args.valueFrom > 0 && fromWalletBalance >= args.valueFrom) {

                fromWallet.setBalance(Number((fromWalletBalance - args.valueFrom).toFixed(2)));
                toWallet.setBalance(Number((toWalletBalance + args.valueTo).toFixed(2)));

                fromWallet.setTransaction({
                    amount: args.valueFrom,
                    currency: args.currencyFrom,
                    exchangedAmount: args.valueTo,
                    exchangedCurrency: args.currencyTo,
                    timestamp: Date.now(),
                    transactionId: fromWallet.getGeneratedTransactionId()
                });

                toWallet.setTransaction({
                    amount: args.valueFrom,
                    currency: args.currencyFrom,
                    exchangedAmount: args.valueTo,
                    exchangedCurrency: args.currencyTo,
                    timestamp: Date.now(),
                    transactionId: toWallet.getGeneratedTransactionId()
                });

                walletsClone.set(args.currencyFrom, fromWallet);
                walletsClone.set(args.currencyTo, toWallet);
            }

            dispatch(saveWallet(walletsClone.getWalletsObject()));
        }

    }, [dispatch, wallets]);

    return (
        <ExchangeView
            wallets={wallets}
            symbols={symbols.symbols || {}}
            onExchange={memoOnExchange}
            showLoader={symbols.isFetching}
            currenciesRate={currenciesRate}
            selectedCurrencyTo={selectedCurrency.to}
            selectedCurrencyFrom={selectedCurrency.from}
            onCurrencyChangeTo={memorizeHandleOnCurrencyChange("to")}
            onCurrencyChangeFrom={memorizeHandleOnCurrencyChange("from")}
        />
    );
};

export default Exchange;