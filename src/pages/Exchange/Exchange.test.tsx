import React from "react";
import ReactDOM from "react-dom";
import {HashRouter as Router} from "react-router-dom";
import Exchange from "./Exchange";
import App from "../../containers/App/App";

it("renders without crashing", () => {
    const div = document.createElement("div");
    ReactDOM.render(
        <App>
            <Router>
                <Exchange/>
            </Router>
        </App>,
        div
    );
    ReactDOM.unmountComponentAtNode(div);
});
