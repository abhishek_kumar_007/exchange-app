import fetchMock from "fetch-mock";
import thunk from "redux-thunk";
import * as actions from "./currenciesRate";
import configureMockStore from "redux-mock-store"
import BaseCommunicator, {BaseURL} from "../communicators/BaseCommunicator";

const mockStore = configureMockStore([thunk]);
const baseUrl: BaseURL = process.env.REACT_APP_EXCHANGE_GENERATE_BASE_URL;

describe("async actions", () => {
    afterEach(() => {
        fetchMock.restore()
    });
    it("Checking fetch currency rate action and its API", () => {

        const url = BaseCommunicator.resolveURL(baseUrl, "/latest/USD");
        const urlStr = BaseCommunicator.urlWithSearchParams(url, null);

        //mocking exchange rate API
        fetchMock.getOnce(urlStr, {
            body: {base: "USD", rates: {"INR": 70.00}},
            headers: {"content-type": "application/json"}
        });

        const expectedActions = [
            {type: actions.REQUEST_CURRENCIES_RATE},
            {type: actions.RECEIVE_CURRENCIES_RATE, currenciesRate: {base: "USD", rates: {"INR": 70.00}}}
        ];

        const store = mockStore({currenciesRate: {}});

        return store.dispatch<any>(actions.fetchCurrenciesRate({baseCurrency: "USD"})).then(() => {
            expect(store.getActions()).toEqual(expectedActions)
        })
    })
});