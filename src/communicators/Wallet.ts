/**
 * Wallet Communicator to fetch user's wallets
 */
import {WalletOptions} from "../models/Wallet";

export type WalletsResponse = { [currency: string]: WalletOptions };

class Wallet {

    /**
     * Returning mock data for user's wallets.
     * Ideally it should be fetch from API
     */
    getMockWalletData(): WalletsResponse {
        const defaultWallets: WalletsResponse = {};

        let usdWallet = {
            currency: "USD",
            balance: 100,
            transactions: []
        };

        let eurWallet = {
            currency: "EUR",
            balance: 500,
            transactions: []
        };

        let gbpWallet = {
            currency: "GBP",
            balance: 190,
            transactions: []
        };

        let inrWallet = {
            currency: "INR",
            balance: 80,
            transactions: []
        };

        defaultWallets["USD"] = usdWallet;
        defaultWallets["EUR"] = eurWallet;
        defaultWallets["GBP"] = gbpWallet;
        defaultWallets["INR"] = inrWallet;

        return defaultWallets;
    }

    /**
     * Fetches user's wallets details
     */
    getWallets = async (): Promise<WalletsResponse> => {
        return this.getMockWalletData();
    };
}

export default Wallet;