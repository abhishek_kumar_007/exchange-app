import React, {FC} from "react";
import {WalletTransaction} from "../../../models/Wallet";
import TransactionListItem from "./TransactionListItem";
import styles from "./transactionList.module.scss";

export interface Props {
    /**
     * supports 3 digit code
     */
    selectedCurrency: string,
    transactions: Array<WalletTransaction>
}

const TransactionList: FC<Props> = (props: Props) => {
    const {
        transactions,
        selectedCurrency
    } = props;

    return (
        <React.Fragment>
            {
                transactions.map((transaction) => {
                    return (
                        <div key={transaction.transactionId}>
                            <TransactionListItem
                                selectedCurrency={selectedCurrency}
                                transaction={transaction}
                            />
                        </div>
                    )
                })
            }
            {
                transactions.length === 0 ?
                    (
                        <div className={styles.noRecordFoundContainer}>
                            <div>No transaction(s) found</div>
                        </div>
                    ) : null
            }
        </React.Fragment>
    )
};

TransactionList.defaultProps = {
    transactions: [],
};

export default React.memo(TransactionList);