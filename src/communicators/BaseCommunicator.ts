/**
 * BaseCommunicator class which supports GET and POST Http methods.
 */

export type BaseURL = string | null | undefined;

class BaseCommunicator {
    baseURL: string = "";

    constructor(baseURL: BaseURL) {
        if (baseURL) {
            this.baseURL = baseURL;
        }
    }

    protected async fetch<TResult>(request: RequestInfo, initOptions?: RequestInit): Promise<TResult> {
        return fetch(request, initOptions).then((response) => response.json())
    };

    static resolveURL(baseURL: BaseURL, path: string): URL {
        if (path.startsWith('/')) {
            path = path.slice(1);
        }

        if (baseURL) {
            const normalizedBaseURL = baseURL.endsWith('/')
                ? baseURL
                : baseURL.concat('/');
            return new URL(path, normalizedBaseURL);
        } else {
            return new URL(path);
        }
    }

    static urlWithSearchParams(url: URL, params: URLSearchParams | null): string {
        if (params) {
            url.search = params.toString();
        }
        return url.toJSON();
    };

    /**
     * Get Http method
     *
     * @param path
     * @param params
     * @param init
     */
    protected async get<TResult = any>(
        path: string,
        params: URLSearchParams | null,
        init?: RequestInit,
    ): Promise<TResult> {

        const url = BaseCommunicator.resolveURL(this.baseURL, path);
        const urlStr = BaseCommunicator.urlWithSearchParams(url, params);

        return this.fetch(
            urlStr,
            {
                method: "GET",
                headers: {
                    ...(init?.headers ? init.headers : {})
                },
                ...init
            }
        );
    }
}

export default BaseCommunicator;