import {Dispatch} from "react";
import {RootState} from "../reducers";
import {BaseReduxAction} from "../types";
import WalletCommunicator, {WalletsResponse} from "../communicators/Wallet";

export interface WalletsAction extends BaseReduxAction {
    wallets?: WalletsResponse,
    updatedWallets?: WalletsResponse
}

export const RECEIVE_WALLETS = "RECEIVE_WALLETS";
export const SAVE_WALLET = "SAVE_WALLET";

const walletCommunicator = new WalletCommunicator();

export function receiveWallets(wallets: WalletsResponse): WalletsAction {
    return {
        type: RECEIVE_WALLETS,
        wallets
    }
}

export function saveWalletAction(updatedWallets: WalletsResponse): WalletsAction {
    return {
        type: SAVE_WALLET,
        updatedWallets
    }
}

export function saveWallet(updatedWallets: WalletsResponse) {
    return async (dispatch: Dispatch<Object>) => {
        dispatch(saveWalletAction({...updatedWallets}));
    }
}

export function fetchWalletsFromServer() {
    return async (dispatch: Dispatch<Object>) => {
        const response = await walletCommunicator.getWallets();
        if (response) {
            dispatch(receiveWallets(response));
        }
    }
}

function shouldFetchWallet(store: RootState) {
    return !store.wallets || !store.wallets.wallets;
}

/**
 * This method won't be used in real scenario
 * as we need to fetch updated data from the server such that we have current user wallet info.
 *
 */
export function fetchWallets() {
    return (dispatch: Dispatch<Object>, getStore: Function) => {
        if (shouldFetchWallet(getStore())) {
            return dispatch(fetchWalletsFromServer())
        }
    }
}