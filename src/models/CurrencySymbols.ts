export type Symbols = { [key: string]: string };

class CurrencySymbols {
    _symbols: Symbols;

    constructor(symbols: Symbols) {
        this._symbols = symbols;
    }

    getSymbols(): Symbols {
        return this._symbols;
    }
}

export default CurrencySymbols;