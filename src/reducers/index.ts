import {combineReducers} from "@reduxjs/toolkit";
import wallets from "./wallet";
import currencySymbols from "./currencySymbols";
import currenciesRate from "./currenciesRate";

const rootReducer = () =>
    combineReducers({
        currencySymbols,
        currenciesRate,
        wallets
    });

export type RootState = ReturnType<ReturnType<typeof rootReducer>>;
export default rootReducer