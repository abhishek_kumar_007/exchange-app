import fetchMock from "fetch-mock";
import thunk from "redux-thunk";
import * as actions from "./currencySymbols";
import configureMockStore from "redux-mock-store"
import BaseCommunicator, {BaseURL} from "../communicators/BaseCommunicator";

const mockStore = configureMockStore([thunk]);

const baseUrl: BaseURL = process.env.REACT_APP_FIXER_BASE_URL;
const accessKey = process.env.REACT_APP_FIXER_ACCESS_KEY;

describe("async actions", () => {
    afterEach(() => {
        fetchMock.restore()
    });
    it("Checking fetch symbols action and its API", () => {

        const url = BaseCommunicator.resolveURL(baseUrl, "/symbols");
        const params = new URLSearchParams();
        params.append("access_key", String(accessKey));

        const urlStr = BaseCommunicator.urlWithSearchParams(url, params);

        // Mocking Symbols API
        fetchMock.getOnce(urlStr, {
            body: {success: true, symbols: {"INR": "INR"}},
            headers: {"content-type": "application/json"}
        });

        const expectedActions = [
            {type: actions.REQUEST_SYMBOLS},
            {type: actions.RECEIVE_SYMBOLS, symbols: {"INR": "INR"}}
        ];

        const store = mockStore({currencySymbols: {symbols: {}}});

        return store.dispatch<any>(actions.fetchSymbols()).then(() => {
            // return of async actions
            expect(store.getActions()).toEqual(expectedActions)
        })
    })
});